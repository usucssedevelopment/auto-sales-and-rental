# auto-sales-and-rental

This repository contains analysis and design models that illustrate the progression from an analysis model to a design model and then to a database design model, in the context of a system that tracks vehicle sales and rentals.

| Model         | Description                          |
+---------------|--------------------------------------|
| Analysis.vpp  | Analysis Model                       |
| Design-01.vpp | Intial Design Model, but incomplete  |
|---------------|--------------------------------------|


